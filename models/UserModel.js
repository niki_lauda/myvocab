/**
 * Created by yevhenii on 10/15/16.
 */

var mongoose  = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;
const SALT_WORK_FACTOR = 10;

var userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    name:{
        type: String
    }
});

userSchema.pre('save', function(next){
    var user = this;

    if(!user.isModified('password')){
        return next();
    }

    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
        if(err){
            return next(err);
        }

        bcrypt.hash(user.password, salt, function(err, hash){
            if(err){
                return next(err);
            }

            user.password = hash;
            next();
        });
    });
});

module.exports = mongoose.model('User', userSchema);