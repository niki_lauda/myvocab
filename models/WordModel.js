/**
 * Created by yevhenii on 10/16/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var wordSchema = new Schema({
    word: {
        type: String,
        required: true
    },
    translation: {
        type: String
    },
    explanation: {
        type: String
    },
    useContext:{
        type: String
    },
    synonyms: [{
        type: String
    }],
    image: {
        type: Buffer
    },
    userID: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Word', wordSchema);