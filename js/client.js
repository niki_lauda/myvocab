/**
 * Created by yevhenii on 11/15/16.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import Layout from './components/page/Layout';
import Home from './pages/Home';
import Login from './pages/Login';
import Registration from './pages/Registration';

const app = document.getElementById('app');

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path='/' component={Layout}>
            <IndexRoute component={Home}/>
            <Route path='login' component={Login}/>
            <Route path='registration' component={Registration}/>
        </Route>
    </Router>, app);