/**
 * Created by yevhenii on 11/17/16.
 */
import React from 'react';

export default class Password extends React.Component{
    render(){
        return(
            <div className="row">
                <div className="input-field col s12">
                    <input id="email" type="email" className="validate"/>
                    <label for="email">{this.props.label || 'Email'}</label>
                </div>
            </div>
        )
    }
}