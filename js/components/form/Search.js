/**
 * Created by yevhenii on 11/17/16.
 */
import React from 'react';

export default class Search extends React.Component {
    render() {
        const navStyle = {
            marginTop: 10
        };

        return (
            <nav style={navStyle}>
                <div className="nav-wrapper grey darken-2">
                    <form>
                        <div className="input-field">
                            <input id="search" type="search" placeholder="Type a word here..." required/>
                            <label htmlFor="search"><i className="material-icons">search</i></label>
                            <i className="material-icons">close</i>
                        </div>
                    </form>
                </div>
            </nav>
        )
    }
}