/**
 * Created by yevhenii on 11/17/16.
 */
import React from 'react';

export default class Password extends React.Component{
    render(){
        return(
            <div className="row">
                <div className="input-field col s12">
                    <input id="password" type="password" className="validate"/>
                    <label for="password">Password</label>
                </div>
            </div>
        )
    }
}