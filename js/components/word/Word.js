/**
 * Created by yevhenii on 11/17/16.
 */
import React from 'react';
import WordRow from './WordRow';
import WordTitle from './WordTitle';
import Image from './Image';


export default class Word extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            image: 'https://static1.squarespace.com/static/528fc8ffe4b070ad8ee97493/t/558c019ae4b08aeb04726a36/1435238813669/Ladybug+Saying+Hello%21?format=1000w',
            wordOriginal: 'Hello!',
            wordExplanation: 'Form of greeting',
            useContext: 'Hello! How are you today? Welcome to this web-app!',
            synonyms: ['Hi', 'Cheers', 'Good day'],
            translation: 'Привіт!'
        }
    }

    render() {
        return (
            <div className="col s12 m7">
                <div className="card horizontal">
                    <Image image={this.state.image}/>
                    <div className="card-stacked">
                        <div className="card-content">
                            <WordTitle data={this.state.wordOriginal}/>
                            <WordRow name="Explanation" data={this.state.wordExplanation}/>
                            <WordRow name="Example" data={this.state.useContext}/>
                            <WordRow name="Synonyms" data={this.state.synonyms.toString()}/>
                            <WordRow name="Translation" data={this.state.translation}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}