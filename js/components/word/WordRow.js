/**
 * Created by yevhenii on 11/17/16.
 */
import React from 'react';

export default class WordRow extends React.Component {
    render() {
        return (
            <div>
                <label><h6>{this.props.name}</h6></label>
                <span>{this.props.data || 'WhoooOps! A bug?'}</span>
            </div>
        );
    }
}