/**
 * Created by yevhenii on 11/17/16.
 */
import React from 'react';

export default class Image extends React.Component {
    constructor(props) {
        super(props);
        this.image = this.props.image;

        if (!this.image) {
            this.image = 'http://www.freeiconspng.com/uploads/no-image-icon-23.jpg';
        }

        this.divStyle = {
            width: 300,
            height: 200,
            overflow: 'hidden'
        };

        this.imgStyle = {
            maxHeight: '100%',
            maxWidth: '100%'
        }
    }

    render() {
        return (
            <div className="card-image" style={this.divStyle}>
                <img src={this.image} style={this.imgStyle}/>
            </div>
        );
    }
}