/**
 * Created by yevhenii on 11/16/16.
 */
import React from 'react';
import {Link} from 'react-router';

export default class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.logoName = 'Home';
        this.homeUrl = '/';
        this.loginUrl = 'login';
        this.registrationUrl = 'registration';
    }

    render() {
        return (
            <nav>
                <div className={"nav-wrapper teal darken-4"}>
                    <div className={"row"}>
                        <div className={"col 4"}>
                            <Link to={this.homeUrl} className={"brand-logo"}>{this.logoName}</Link>
                            <a href="#" data-activates={"mobile-demo"} className={"button-collapse"}><i
                                className={"material-icons"}>reorder</i></a>
                        </div>
                        <div >
                            <ul className={"right hide-on-med-and-down"}>
                                <li><Link to={this.loginUrl}>Login</Link></li>
                                <li><Link to={this.registrationUrl}>Registration</Link></li>
                            </ul>
                            <ul className={"side-nav"} id="mobile-demo">
                                <li><Link to={this.loginUrl}>Login</Link></li>
                                <li><Link to={this.registrationUrl}>Registration</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}