/**
 * Created by yevhenii on 11/15/16.
 */
import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Navbar from './Navbar';

export default class Layout extends React.Component {
    render() {
        return (
            <div className={"page-flexbox-wrapper"}>
                <Header/>
                <Navbar/>
                <main>
                    <div className='container'>
                        {this.props.children}
                    </div>
                </main>
                <Footer/>
            </div>
        )
    }
}
