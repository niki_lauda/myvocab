/**
 * Created by yevhenii on 11/15/16.
 */
import React from 'react';

export default class Footer extends React.Component {
    render() {
        return (
            <footer className={"page-footer teal lighten-2"}>
                <div className={"container"}>
                    <div className={"row"}>
                        <div className={"col l6 s12"}>
                            <h5 className={"white-text"}>myvocab</h5>
                            <p className={"grey-text text-lighten-4"}>It's a university diploma project powered by
                                Node.js and React.js Technologies</p>
                        </div>
                        <div className={"col l4 offset-l2 s12"}>
                            <h5 className={"white-text"}>Useful information</h5>
                            <ul>
                                <li><a className={"grey-text text-lighten-3"} href="#!">FAQ</a></li>
                                <li><a className={"grey-text text-lighten-3"} href="#!">About</a></li>
                                <li><a className={"grey-text text-lighten-3"} href="#!">Contact</a></li>
                                <li><a className={"grey-text text-lighten-3"} href="#!">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className={"footer-copyright"}>
                    <div className={"container"}>
                        © 2016 Niki Lauda
                        <label className={"grey-text text-lighten-4 right"}>All rights reserved</label>
                    </div>
                </div>
            </footer>
        )
    }
}

