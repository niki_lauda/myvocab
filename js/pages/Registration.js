/**
 * Created by yevhenii on 11/16/16.
 */

import React from 'react';
import PasswordField from '../components/form/PasswordField';
import EmailField from '../components/form/EmailField';


export default class Registration extends React.Component {
    render() {
        return (
            <div className="row" style={{marginTop: '30px'}}>
                <form className="col s12">
                    <EmailField label="Email"/>
                    <PasswordField label="Password"/>
                    <PasswordField label="Verify Password"/>
                    <div className="row">
                        <div className="input-field col s12 m6">
                            <select className="icons">
                                <option value="" disabled selected>Select language to translate into</option>
                                <option value=""
                                        data-icon="https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1280px-Flag_of_the_United_Kingdom.svg.png"
                                        className="left circle">example 1
                                </option>
                                <option value="" data-icon="images/office.jpg" class="left circle">example 2</option>
                                <option value="" data-icon="images/yuna.jpg" class="left circle">example 3</option>
                            </select>
                            <label>Click to select</label>
                        </div>
                        <div className="input-field col s12 m6">
                            <select className="icons">
                                <option value="" disabled selected>Select language to translate into</option>
                                <option value=""
                                        data-icon="https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1280px-Flag_of_the_United_Kingdom.svg.png"
                                        className="left circle">example 1
                                </option>
                                <option value="" data-icon="images/office.jpg" class="left circle">example 2</option>
                                <option value="" data-icon="images/yuna.jpg" class="left circle">example 3</option>
                            </select>
                            <label>Click to select</label>
                        </div>
                    </div>
                    <div className="row">
                        <button className=" btn-large waves-effect waves-light" type="submit" name="action">Register
                            <i className="material-icons right">send</i>
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}