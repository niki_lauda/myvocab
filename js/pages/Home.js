/**
 * Created by yevhenii on 11/16/16.
 */
import React from 'react';
import Search from '../components/form/Search';
import Word from '../components/word/Word';

export default class Home extends React.Component {
    render() {
        return (
            <div>
                <Search/>
                <Word/>
            </div>
        )
    }
}