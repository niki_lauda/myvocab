module.exports = {
    entry: './public/js/components/header.js',
    output: {
        path: './public/js',
        filename: 'client.bundle.js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query:{
                presets: ['react']
            }
        }]
    }
};
