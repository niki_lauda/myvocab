////////////////////////////////////////////////////////////////////////
//IMPORT SECTION: node_modules
//
var express = require('express');
var handlebars = require('express-handlebars');
var path = require('path');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var app = express();


////////////////////////////////////////////////////////////////////////
//IMPORT SECTION: controllers
//
var homeController = require('./controllers/home');
var wordApi = require('./controllers/api/wordApi');


////////////////////////////////////////////////////////////////////////
//MIDDLEWARES USE:
app.engine('handlebars', handlebars({defaultLayout:'main'}));
app.set('view engine', 'handlebars');
app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'true'}));


////////////////////////////////////////////////////////////////////////
//ROUTES REGISTRATION:
homeController.registerRoutes(app);
wordApi.registerRoutes(app);


app.listen(3000, function onListen(){
    console.log('Server is listening on port 3000!');
});


module.exports = app;
