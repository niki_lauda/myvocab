/**
 * Created by yevhenii on 11/6/16.
 */

var wordApi = require('./controllers/api/wordApi');
var apiDoc = require('./controllers/api/apiDoc');
var homeRoutes = require('./controllers/home');

module.exports.registerRoutes = function(app){

    ////////////////////////////////////////////////////////////////
    //API Word routes
    app.get('/api/explanation/', wordApi.getExplanation);
    app.get('/api/images/', wordApi.getImages);
    app.get('/api/example/', wordApi.getUseContext);
    app.get('/api/synonyms/', wordApi.getSynonyms);
    app.get('/api/translation/', wordApi.getTranslation);

    ////////////////////////////////////////////////////////////////
    //API Word documentation page
    app.get('/api', apiDoc.getApiDoc);

};