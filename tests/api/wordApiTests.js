/**
 * Created by yevhenii on 10/20/16.
 */

var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var supertest = require('supertest');
var app = require('../../index');
var request = supertest(app);

describe('API requestst to /api/words', function(){
    it('/:word should return a word in json with status 200', function(done){
        var requestWord = 'hello';
        request
            .get('/api/words/' + requestWord)
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            .end(function(err, res){
                expect(res.body.word).to.be.equal(requestWord);
                expect(err).to.be.null;
                done();
            });
    });
});
