/**
 * Created by yevhenii on 10/17/16.
 */

var chai = require('chai');
var expect = chai.expect;
var should = chai.should();
var wordActions = require('../../libs/db/wordActions');
var sinon = require('sinon');
var WordModel = require('../../models/WordModel');

describe('Create a word', function () {
    it('should fail if [word] property is not provided', function () {
        var word = new WordModel();
        var err = word.validateSync();
        expect(err).to.exist;
    });

    it('should not fail if other properties [...] are not provided', function () {
        var word = new WordModel();
        word.word = 'hello';
        var err = word.validateSync();
        expect(err).to.not.exist;
    });
});


describe('Get all words', function(){
    it('should pass not empty array to callback', function(){
        //simulate mongooseModel.find({}, callback)
        //should return no error and array of documents
        var find = sinon.stub(WordModel, 'find');
        var data = [{'email':'email', 'password':'password', 'name':'name'}];
        var callback = sinon.spy();
        find.yields(null, data);

        wordActions.list(callback);

        sinon.assert.calledWith(callback, null, data);
        expect(callback.args[0][1]).not.to.be.empty;

        find.restore();
    });

    it('should pass Error to callback if array is empty', function(){
        var find = sinon.stub(WordModel, 'find');
        var callback = sinon.spy();
        find.yields(null, []);

        wordActions.list(callback);

        sinon.assert.calledWith(callback, new Error());

        find.restore();
    })
});


describe('Delete a word', function(){
    it('should pass null to callback if a word is deleted', function(){
        var remove = sinon.stub(WordModel, 'remove');
        var obj = {result: {n : 1}};
        var callback = sinon.spy();

        remove.yields(null, obj);
        wordActions.delete('key', callback);

        expect(callback.args[0]).to.exist;

        remove.restore();
    });

    it('should pass new Error to callback if a word is not deleted', function(){
        var remove = sinon.stub(WordModel, 'remove');
        var obj = {result: {n : 0}};
        remove.yields(null, obj);
        var callback = sinon.spy();

        wordActions.delete('key', callback);
        sinon.assert.calledWith(callback, new Error());

        remove.restore();
    });
});


describe('Read a word', function(){
    it('Should pass null and a word to callback on success', function(){
        var find = sinon.stub(WordModel, 'findOne');
        var word = new WordModel({'word': 'word'});
        find.yields(null, word);
        var callback = sinon.spy();

        wordActions.read('key', callback);
        sinon.assert.calledWith(callback, null, word);

        find.restore();
    });

    it('should pass new Error to callback if a word is not found', function(){
        var find = sinon.stub(WordModel, 'findOne');
        find.yields(null, null);
        var callback = sinon.spy();

        wordActions.read('key', callback);
        sinon.assert.calledWith(callback,new Error());

        find.restore();
    });
});

