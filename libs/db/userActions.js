/**
 * Created by yevhenii on 10/15/16.
 */

var UserModel = require('../../models/UserModel');

module.exports.create = function(data, callback){
    var user = new UserModel({
        email: data.email,
        password: data.password,
        name: data.name
    });
    user.save(function(err, user){
        if(err){
            callback(err);
            return;
        }
        callback(null, user);
    });
};


module.exports.read = function(key, callback){
   UserModel.findOne({email:key}, function(err, user){
       if(err){
           return callback(err);
       }

       if(!user || user === 'undefined'){
           return callback(new Error('UserModel has not been not found!'));
       }

       callback(null, user);
   });
};