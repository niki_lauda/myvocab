/**
 * Created by yevhenii on 10/17/16.
 */

var WordModel = require('../../models/WordModel');

function updateProperties(src, dest){
    for(var property in src){
        if(src.hasOwnProperty(property)){
            if(src[property] !== null && src[property] !== 'undefined'){
                dest.set(property, src[property]);
            }
        }
    }
}


module.exports.create = function(data, callback){
    var word = new WordModel(data);
    word.save(function(err){
        if(err){
            return callback(err);
        }
        callback(null);
    })
};


module.exports.read = function(key, callback){
    WordModel.findOne({word:key}, function(err, word){
        if(err){
            return callback(err);
        }
        if(!word || word === 'undefined'){
            return callback(new Error('Word has not been found!'));
        }
        callback(null, word);
    });
};


module.exports.update = function(key, data, callback){
    WordModel.findOne({word: key},function(err, word){
        if(err){
            return callback(err);
        }

        if(!word || word === 'undefined'){
            return callback(new Error('Word has not been found!'));
        }

        updateProperties(data, word);
        console.log(word);
        word.save(function(err){
            if(err){
                callback(err);
            }
            callback(null);
        });
    })
};


module.exports.delete = function(key, callback){
    WordModel.remove({word: key}, function(err, obj){
        if(err){
            return callback(err);
        }
        if(obj.result.n === 0){
            return callback(new Error('Word has not been deleted!'));
        }
        return callback(null);
    })
};


module.exports.list = function(callback){
    WordModel.find({}, function(err, words){
        if(err){
            return callback(err);
        }
        if(words.length === 0){
            return callback(new Error('List is empty'));
        }
        callback(null, words);
    });
};
