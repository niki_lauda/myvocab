/**
 * Created by yevhenii on 10/15/16.
 */

var mongoose = require('mongoose');

mongoose.Promise = global.Promise;

module.exports.connect = function(uri){

    if(!uri || typeof uri !== 'string' || uri.length < 1){
        console.log('Mongoose connect failed: uri argument is invalid');
        return;
    }


    mongoose.connection.on('connected', function(){
        console.log('Mongoose default connection open to ' + uri);
    });


    mongoose.connection.on('error', function(err){
        console.log('Mongoose default connection error: ' + err);
    });


    mongoose.connection.on('disconnected', function(){
        console.log('Mongoose default connection disconnected');
    });


    process.on('SIGINT', function(){
        mongoose.connection.close(function(){
            console.log('Mongoose default connection disconnected through app termination.');
            process.exit(0);
        })
    });

    mongoose.connect(uri);

    return mongoose.connection;
};

