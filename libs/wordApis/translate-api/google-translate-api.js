/**
 * Created by yevhenii on 11/6/16.
 */
//getT ranslation for a word
//using Google API Translate
//https://github.com/matheuss/google-translate-api

var translate = require('google-translate-api');

module.exports.getTranslation = function(word, from, to, callback){
    translate(word, {from: from, to: to}).then(res=>{
        callback(null, res.text);
    }).catch(err =>{
        callback(err);
    })
};