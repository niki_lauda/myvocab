/**
 * Created by yevhenii on 11/6/16.
 */
//get image for a word
//using images API
//https://pixabay.com/api/

var request = require('request-json');

var baseUrl = 'https://pixabay.com/api/?';
var key = '3696198-a6d8f0f1d20ef56d770bb3035';

var params = {
    per_page : 3
};


function constructUrl(word){
    var url = baseUrl;

    url += 'key=' + key + '&q=' + word;

    for(var field in params){
        url += '&' + field + '=' + params[field];
    }

    return url;
}

function apiCall(url, callback){
    var client = request.createClient(url);

    client.get(url, function(err, res, body){
        if(err){
            return callback(err);
        }

        callback(null, body);
    });
}

module.exports.getImages = function(word, callback){
    var url = constructUrl(word);
    apiCall(url, function(err, body){
        if(err){
            return callback(err);
        }

        var resultsCount = body.hits.length;
        var urls = [];

        for(var i = 0; i < resultsCount; i++){
            urls.push(body.hits[i].webformatURL);
        }

        callback(null, urls);
    });
};