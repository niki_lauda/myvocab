/**
 * Created by yevhenii on 11/5/16.
 */
//get word explanation, use context and synonyms
//Documentation URL
//http://developer.wordnik.com/docs.html
//API URL
//http://api.wordnik.com:80/v4/word.json/

var request = require('request-json');

var baseUrl = 'http://api.wordnik.com:80/v4/word.json/';

var api_key = 'a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5';


function apiCall(url, callback){
    var client = request.createClient(url);

    client.get(url, function(err, res, body){
       if(err){
           return callback(err);
       }

       if(res.statusCode !== 200){
           return callback(new Error('Error! Status code: ' + res.statusCode));
       }

       callback(null, body);
    });
}


function constructUrl(word, searchType, params){
    var fullUrl = baseUrl;

    fullUrl += word + '/' + searchType + '?';

    for(var field in params){
        fullUrl += field + '=' + params[field] + '&';
    }

    return fullUrl + 'api_key=' + api_key;
}


module.exports.getExplanation = function(word, callback){
    var params = {
        limit: 1,
        includeRelated: false,
        sourceDictionaries: 'all',
        useCanonical: false,
        includeTags: false,
    };

    var url = constructUrl(word, 'definitions', params);

    apiCall(url, function(err, body){
        if(err){
            return callback(err);
        }
        if(!body || !body[0] || !body[0].text){
            return callback(new Error('No explanation has been found!'));
        }
        callback(null, body[0].text);
    })
};


module.exports.getUseContext = function(word, callback){
    var params = {
        includeDuplicates: false,
        useCanonical: false,
        skip: 0,
        limit: 1
    };

    var url = constructUrl(word, 'examples', params);

    apiCall(url, function(err, body){
        if(err){
            return callback(err);
        }
        console.log(body);
        if(!body || !body.examples || ! body.examples[0].text){
            return callback(new Error('No use context has been found!'))
        }
        callback(null, body.examples[0].text);
    });
};


module.exports.getSynonyms = function(word, callback){
    var params = {
        useCanonical: false,
        relationshipTypes: 'synonym',
        limitPerRelationshipType: 5
    };

    var url = constructUrl(word, 'relatedWords', params);

    apiCall(url, function(err, body){
        if(err){
            return callback(err);
        }
        if(!body || !body[0] || !body[0].words){
            return callback(new Error('No synonyms have been found!'));
        }

        callback(null, body[0].words);
    });
};