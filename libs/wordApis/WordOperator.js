/**
 * Created by yevhenii on 11/6/16.
 */

var DictFactory = require('./DictFactory');
var pixabay = require('./image-api/pixabay');
var translate = require('./translate-api/google-translate-api');

module.exports.getOperator = function(originLanguage='en'){
    var dict = DictFactory.getDict(originLanguage);
    return {
        getExplanation: dict.getExplanation,
        getUseContext: dict.getUseContext,
        getSynonyms: dict.getSynonyms,
        getImages: pixabay.getImages,
        getTranslation: translate.getTranslation
    }
};