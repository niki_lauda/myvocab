/**
 * Created by yevhenii on 10/14/16.
 */
var WordOperator = require('../../libs/wordApis/WordOperator');


module.exports.getExplanation = function(req, res){
    var word = req.query.word;
    var langSrc = req.query.langSrc;

    var operator = WordOperator.getOperator(langSrc);

    operator.getExplanation(word, function(err, data){
        if(err){
            return res.json({error: err.message});
        }
        res.json({explanation: data});
    })
};


module.exports.getImages = function(req, res){
    var word = req.query.word;
    var langSrc = req.query.langSrc;
    var operator = WordOperator.getOperator(langSrc);

    operator.getImages(word, function(err, data){
        if(err){
            return res.json({error: err.message});
        }
        res.json({images: data});
    })
};


module.exports.getUseContext = function(req, res){
    var word = req.query.word;
    var langSrc = req.query.langSrc;

    var operator = WordOperator.getOperator(langSrc);

    operator.getUseContext(word, function(err, data){
        if(err){
            return res.json({error: err.message});
        }
        res.json({useContext: data});
    })
};


module.exports.getSynonyms = function(req, res){
    var word = req.query.word;
    var langSrc = req.query.langSrc;

    var operator = WordOperator.getOperator(langSrc);

    operator.getSynonyms(word, function(err, data){
        if(err){
            console.log(err);
            return res.json({error: err.message});
        }
        res.json({synonyms: data});
    })
};


module.exports.getTranslation = function(req, res){
    var word = req.query.word;
    var langSrc = req.query.langSrc;
    var langDest = req.query.langDest;

    var operator = WordOperator.getOperator(langSrc);

    operator.getTranslation(word, langSrc, langDest, function(err, data){
        if(err){
            return res.json({error: err.message});
        }
        res.json({translation: data});
    })
};