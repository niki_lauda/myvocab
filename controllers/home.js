module.exports.registerRoutes = registerRoutes;

function registerRoutes(app){
    app.get('/', home);
}

function home(req, res){
    res.render('home');
}
